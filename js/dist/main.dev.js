"use strict";

$(document).ready(function () {
  $(".header__hamburger").click(function () {
    $(this).toggleClass("show-btn");
    $(".nav").toggleClass("show-menu");
    $(".nav").toggleClass("swing-in-top-fwd");
  });
  $(".shop-link-div").hover(function () {
    $(this).remove(".shop-now-div");
    var span = "<div class='shop-now-div'><span class='icon-bicycle'></span><span class='shop-now'>SHOP NOW</span></div>";
    $(this).toggleClass("hover");
    $(this).html(span);
  });
});